(function(gg, $, undefined) {
    gg.markers = [];
    gg.mode = 'view';
    gg.routePointsSnd=[];
    gg.markersSnd=[];

    gg.initialize = function() {
        var lat = $.cookie('lat') * 1;
        var lng = $.cookie('lng') * 1;
        var zoom = $.cookie('zoom') * 1;

        var initialLocation = new google.maps.LatLng(55.755826, 37.6173);
        if (isNaN(lat && lng && zoom)) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(initialLocation);
                }, function() {
                    map.setCenter(initialLocation);
                });
            }
        } else {
            initialLocation = new google.maps.LatLng(lat, lng);
        }

        var mapOptions = {
            center: initialLocation,
            zoom: zoom || 13
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
        gg.map = map;
        google.maps.event.addListener(map, 'idle', gg.setCookies);
        var modes = $('.btn-toolbar')[0];
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);
        $('#mode-selector').removeClass('hidden').addClass('show');
        $('#del-route').click(gg.delRoute);

        $('input[name="obj-mode"]').change(function() {
            if ($(this).parent().hasClass('active')) {
                setTimeout("$('.btn.hidden').click()", 10);
            } else {
                gg[$(this).val()]();
            }

        });


        var input = $('#pac-input')[0];
        var autocomplete = new google.maps.places.Autocomplete(input, {types: ['(regions)']});
        autocomplete.bindTo('bounds', map);


        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(13);
            }
        });

        gg.infoWindow = new google.maps.InfoWindow();
        var routeOptions = {
            strokeColor: '#0000A0',
            strokeOpacity: 1.0,
            strokeWeight: 3
        };
        gg.routeLine = new google.maps.Polyline(routeOptions);
        gg.routeLine.setMap(gg.map);
    };

    gg.stop = function() {
        gg.mode = 'editMarker';
        $('#del-route').removeClass('show').addClass('hidden');
        google.maps.event.clearListeners(gg.map, 'click');
        gg.map.setOptions({draggableCursor: "url(http://maps.gstatic.com/intl/en_ALL/mapfiles/drag_cross_67_16.png) 8 8, auto"});

        google.maps.event.addListener(gg.map, 'click', function(event) {
            gg.infoWindow.close();
            var marker = new google.maps.Marker({
                position: event.latLng,
                map: gg.map
            });
            gg.markersSnd.push({latLng: {lat: event.latLng.lat(), lng: event.latLng.lng()}, descr: ''});
            $('#markers').val(JSON.stringify(gg.markersSnd));
            gg.markers.push(marker);
            google.maps.event.addListener(marker, 'click', gg.makerClick);
        });
    };
    
   gg.makerClick= function() {
                var markInd = gg.getMarkerInd(this);
                var txt = (gg.mode === 'view') ? "<p>" + gg.markersSnd[markInd].descr + "</p>" : "<p><textarea id='descr" + markInd + "' class='txtInfo' rows=5 cols=30></textarea></p><p><button id='btn" + markInd + "' type='button' class='marker-del-btn btn btn-danger btn-xs center-block'><span class='glyphicon glyphicon-trash'></span></button></p>";
                gg.infoWindow.setContent(txt);
                gg.infoWindow.open(gg.map, marker);
                google.maps.event.clearListeners(gg.infoWindow, 'domready');
                if (gg.mode === 'view') {
                    return;
                }
                $(document).on('input propertychange', "#descr" + markInd, function() {
                    gg.markersSnd[markInd].descr = $(this).val();
                    $('#markers').val(JSON.stringify(gg.markersSnd));
                });

                google.maps.event.addListener(gg.infoWindow, 'domready', function() {
                    $("#descr" + markInd).val(gg.markersSnd[markInd].descr);
                    $("#btn" + markInd).click(function() {
                        gg.infoWindow.close();
                        gg.markers[markInd].setMap(null);
                        gg.markers.splice(markInd, 1);
                        gg.markersSnd.splice(markInd, 1);
                        $('#markers').val(JSON.stringify(gg.markersSnd));
                    });
                });
            };
            
    gg.hide = function() {
        gg.mode = 'view';
        gg.infoWindow.close();
        google.maps.event.clearListeners(gg.map, 'click');
        gg.map.setOptions({draggableCursor: undefined});
        $('#del-route').removeClass('show').addClass('hidden');
    };
    gg.route = function() {
        gg.mode = 'editRoute';
        gg.infoWindow.close();
        google.maps.event.clearListeners(gg.map, 'click');
        $('#del-route').removeClass('hidden').addClass('show');
        gg.map.setOptions({draggableCursor: 'crosshair'});

        google.maps.event.addListener(gg.map, 'click', function(event) {
            var path = gg.routeLine.getPath();
            path.push(event.latLng);
            if (path.getLength() === 1) {
                gg.firstPoint();
            }
            var obj = {lat: event.latLng.lat(), lng: event.latLng.lng()};
            gg.routePointsSnd.push(obj);
            $('#route').val(JSON.stringify(gg.routePointsSnd));
            
        });
    };

    gg.getMarkerInd = function(marker) {
        var ret = -1;
        var l = gg.markersSnd.length;
        var pos = marker.getPosition();
        for (var i = 0; i < l; i++) {
            if ((gg.markersSnd[i].latLng.lat === pos.lat()) && (gg.markersSnd[i].latLng.lng === pos.lng())) {
                ret = i;
                break;
            }
        }
        return ret;
    };

    gg.firstPoint = function() {
        var country = "";
        var city = "";
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': gg.routeLine.getPath().getAt(0)}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                for (var j = 0; j < results[0].address_components.length; j++) {
                    if ($.inArray("country", results[0].address_components[j].types) >= 0) {
                        country = results[0].address_components[j].long_name;
                    }
                    if ($.inArray("locality", results[0].address_components[j].types) >= 0) {
                        city = results[0].address_components[j].long_name;
                    }
                    if ((country !== "") && (city !== "")) {
                        break;
                    }

                }
            }
            $('#country').val(country);
            $('#city').val(city);
            $('.route-send').removeClass('disabled');
        });
    };

    gg.delRoute = function() {
        gg.routeLine.setPath([]);
        gg.routePointsSnd=[];
        $('.route-send').addClass('disabled');
        $('#country').val('');
        $('#city').val('');
       
    };

    gg.pathReleased = function() {
        if (gg.routeLine.getPath().getLength() > 1) {
            $('.route-send').removeClass('disabled');
        } else {
            $('.route-send').addClass('disabled');
        }
    };

    gg.setCookies = function() {
        var center = gg.map.getCenter();
        if (!center) {
            return;
        }
        $.cookie('lat', center.lat());
        $.cookie('lng', center.lng());
        $.cookie('zoom', gg.map.getZoom());
    };
    
    google.maps.event.addDomListener(window, 'load', gg.initialize);
}(window.gg = window.gg || {}, jQuery));
