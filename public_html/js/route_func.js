(function(gg, $, undefined) {

    gg.stop = function() {
        gg.mode = 'editMarker';
        gg.releaseRoute();
        gg.infoWindow.close();
        google.maps.event.clearListeners(gg.map, 'click');
        gg.map.setOptions({draggableCursor: "url(http://maps.gstatic.com/intl/en_ALL/mapfiles/drag_cross_67_16.png) 8 8, auto"});
        for (i = 0; i < gg.markers.length; i++) {
            gg.markers[i].setOptions({draggable: true})
        }
        google.maps.event.addListener(gg.map, 'click', function(event) {
            var newMarker = gg.createMarker(event.latLng, '');
            newMarker.setOptions({draggable: true});
        });
    };

    gg.createMarker = function(position, descr) {
        
        gg.infoWindow.close();
        var marker = new google.maps.Marker({
            position: position,
            map: gg.map,
            icon: 'images/marker.png'
        });
        gg.markersSnd.push({latLng: {lat: position.lat(), lng: position.lng()}, descr: descr});
        $('#markers').val(JSON.stringify(gg.markersSnd));
        gg.markers.push(marker);
        var stopname = descr||'<остановка>';
        $('.stop-container').append('<div class="well well-sm tile" id=markQ'+(gg.markers.length-1)+'>'+stopname+'</div>')
        google.maps.event.addListener(marker, 'click', gg.makerClick);
        google.maps.event.addListener(marker, 'dragstart', function(event) {
            gg.oldPos = event.latLng;
        });
        google.maps.event.addListener(marker, 'dragend', function(event) {
            var markInd = gg.getMarkerInd(this);
            gg.markersSnd[markInd].latLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
            $('#markers').val(JSON.stringify(gg.markersSnd));
            gg.oldPos = undefined;
        });
        return marker;
    };

    gg.makerClick = function() {
        var markInd = gg.getMarkerInd(this);
        var txt = (gg.mode !== 'editMarker') ? "<p>" + gg.markersSnd[markInd].descr + "</p>" : "<div class='noscrollbar'><p><textarea id='descr" + markInd + "' class='txtInfo' rows=5 cols=30></textarea></p><p><button id='btn" + markInd + "' type='button' class='marker-del-btn btn btn-danger btn-xs center-block'><span class='glyphicon glyphicon-trash'></span></button></p></div>";
        gg.infoWindow.setContent(txt);
        gg.infoWindow.open(gg.map, this);
        google.maps.event.clearListeners(gg.map, 'click');
        gg.map.setOptions({draggableCursor: undefined});
        google.maps.event.addListener(gg.map, 'click', function(event) {
            gg.infoWindow.close();
            google.maps.event.clearListeners(gg.map, 'click');
            gg.map.setOptions({draggableCursor: "url(http://maps.gstatic.com/intl/en_ALL/mapfiles/drag_cross_67_16.png) 8 8, auto"});
            google.maps.event.addListener(gg.map, 'click', function(event) {
                var newMarker = gg.createMarker(event.latLng, '');
                newMarker.setOptions({draggable: true});
            });
        });
        google.maps.event.clearListeners(gg.infoWindow, 'domready');
        if (gg.mode === 'view') {
            return;
        }
        $(document).on('input propertychange', "#descr" + markInd, function() {
            gg.markersSnd[markInd].descr = $(this).val();
            $('#markers').val(JSON.stringify(gg.markersSnd));
            $('#markQ'+markInd).text($(this).val());
        });

        google.maps.event.addListener(gg.infoWindow, 'domready', function() {
            $("#descr" + markInd).val(gg.markersSnd[markInd].descr);
            $("#btn" + markInd).click(function() {
                gg.infoWindow.close();
                gg.markers[markInd].setMap(null);
                $('#markQ'+markInd).remove();
                gg.markers.splice(markInd, 1);
                gg.markersSnd.splice(markInd, 1);
                for (var i = 0; i < gg.markersSnd.length; i++) {
                    $('.tile').eq(i).attr('id','markQ'+i);
                }
                $('#markers').val(JSON.stringify(gg.markersSnd));
            });
        });
    };

    gg.releaseMarkers = function() {
        for (i = 0; i < gg.markers.length; i++) {
            gg.markers[i].setOptions({draggable: false})
        }
        gg.map.setOptions({draggableCursor: undefined});
        google.maps.event.clearListeners(gg.map, 'click');
        gg.infoWindow.close();
    };

    gg.releaseRoute = function() {
        $('#del-route').removeClass('show').addClass('hidden');
        gg.routeLine.setOptions({editable: false});
    };
    gg.routeChanged = function() {
        var path = gg.routeLine.getPath().getArray();
        gg.routePointsSnd = [];
        for (i = 0; i < path.length; i++) {
            var obj = {lat: path[i].lat(), lng: path[i].lng()};
            gg.routePointsSnd.push(obj);
        }
        $('#route').val(JSON.stringify(gg.routePointsSnd));
        gg.setRouteLength();
    };
    gg.deleteNode = function(mev) {
        if (mev.vertex != null) {
            var path = gg.routeLine.getPath();
            path.removeAt(mev.vertex);
            if (path.getLength() === 0) {
                gg.delRoute();
            }
            gg.setRouteLength();
        }
    };

    gg.hide = function() {
        gg.mode = 'view';
        gg.releaseRoute();
        gg.releaseMarkers();
    };

    gg.route = function() {
        gg.mode = 'editRoute';
        gg.releaseMarkers();
        $('#del-route').removeClass('hidden').addClass('show');
        gg.map.setOptions({draggableCursor: 'crosshair'});
        gg.routeLine.setOptions({editable: true});

        google.maps.event.addListener(gg.map, 'click', function(event) {
            var path = gg.routeLine.getPath();
            path.push(event.latLng);
            if (path.getLength() === 1) {
                gg.firstPoint();
            }
            gg.setRouteLength();
        });
    };

    gg.setRouteLength=function(){
        var path = gg.routeLine.getPath();
        var dd = google.maps.geometry.spherical.computeLength(path.getArray())/1000;
        $('.row-length').html(dd.toFixed(2));
        $('#lenght').val(dd.toFixed(2));
    }

    gg.getMarkerInd = function(marker) {
        var ret = -1;
        var l = gg.markersSnd.length;
        var pos = (gg.oldPos === undefined) ? marker.getPosition() : gg.oldPos;
        for (var i = 0; i < l; i++) {
            if ((gg.markersSnd[i].latLng.lat === pos.lat()) && (gg.markersSnd[i].latLng.lng === pos.lng())) {
                ret = i;
                break;
            }
        }
        return ret;
    };

    gg.firstPoint = function() {
        var country = "";
        var city = "";
        gg.routeEditListeners();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': gg.routeLine.getPath().getAt(0)}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                for (var j = 0; j < results[0].address_components.length; j++) {
                    if ($.inArray("country", results[0].address_components[j].types) >= 0) {
                        country = results[0].address_components[j].long_name;
                    }
                    if ($.inArray("locality", results[0].address_components[j].types) >= 0) {
                        city = results[0].address_components[j].long_name;
                    }
                    if ((country !== "") && (city !== "")) {
                        break;
                    }

                }
            }
            $('#country').val(country);
            $('#city').val(city);
            gg.allow2flag = true;
            gg.allowSnd();

        });
    };
    gg.allowSnd = function() {
        if (gg.allow2flag && gg.allow1flag) {
            $('.route-send').removeClass('disabled');
        } else {
            $('.route-send').addClass('disabled');
        }
    };
    gg.routeEditListeners = function() {
        google.maps.event.addListener(gg.routeLine.getPath(), "insert_at", gg.routeChanged);
        google.maps.event.addListener(gg.routeLine.getPath(), "remove_at", gg.routeChanged);
        google.maps.event.addListener(gg.routeLine.getPath(), "set_at", gg.routeChanged);
        google.maps.event.addListener(gg.routeLine, 'rightclick', gg.deleteNode);
    }

    gg.delRoute = function() {
        gg.routeLine.setPath([]);
        gg.routePointsSnd = [];
        gg.allow2flag = false;
        gg.allowSnd();
        $('#country').val('');
        $('#route').val('');
        $('#city').val('');
        google.maps.event.clearListeners(gg.routeLine, 'rightclick');
        gg.setRouteLength();
    };

    gg.setCookies = function() {
        var center = gg.map.getCenter();
        if (!center) {
            return;
        }
        $.cookie('lat', center.lat());
        $.cookie('lng', center.lng());
        $.cookie('zoom', gg.map.getZoom());
    };
    
    gg.stopReorder=function(e,ui){
        gg.new_position = $(this).sortable('toArray');
        var newMarkersSnd=[];
        var newMarkers=[];
        for (i = 0; i < gg.new_position.length; i++) {
            newMarkersSnd[i]=gg.markersSnd[$.inArray(gg.new_position[i],gg.old_position)];
            newMarkers[i]=gg.markers[$.inArray(gg.new_position[i],gg.old_position)];
            $('.tile').eq(i).attr('id','markQ'+i);
        }
        gg.markersSnd=newMarkersSnd;
        gg.markers=newMarkers;
        $('#markers').val(JSON.stringify(gg.markersSnd));
    };
    
    $(".stop-container").sortable({
        tolerance: 'pointer',
        placeholder: 'span2 well placeholder tile',
        forceHelperSize: true,
        start: function(e, ui) {
            gg.infoWindow.close();
            gg.old_position = $(this).sortable('toArray');
            var overind =$.inArray('',gg.old_position)
            if (overind>-1) {
                gg.old_position.splice(overind, 1);
            }
        },
        update: gg.stopReorder
    });
    
    $('#name_route').on("change keyup paste",function(){
        gg.allow1flag=$('#name_route').val().trim()!=="";
        gg.allowSnd();
    });
}(window.gg = window.gg || {}, jQuery));