(function(gg, $, undefined) {
    gg.markers = [];
    gg.mode = 'view';
    gg.routePointsSnd=[];
    gg.markersSnd=[];
    gg.routeField=$("#route" );
    gg.markerField=$("#markers" );
    

    gg.initialize = function() {
        try{
            gg.routeRcv   = $.parseJSON(gg.routeField.val());
            gg.markersRcv = $.parseJSON(gg.markerField.val());
        }catch(e){
            
        };
        gg.allow1flag=true;
        gg.allow2flag=true;
        var initialLocation = new google.maps.LatLng(55.755826, 37.6173);
        var mapOptions = {
            center: initialLocation,
            zoom: zoom || 13
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
                
        gg.map = map;
        gg.infoWindow = new google.maps.InfoWindow();
        
        if(gg.routeRcv!==undefined){
            var wholeBounds = new google.maps.LatLngBounds();
            var routePoints = [];
            for (i = 0; i < gg.routeRcv.length; i++) {
               var point = new google.maps.LatLng(gg.routeRcv[i].lat, gg.routeRcv[i].lng);
                routePoints.push(point);
                wholeBounds.extend(point);
            };
            for (i = 0; i < gg.markersRcv.length; i++) {
                var point = new google.maps.LatLng(gg.markersRcv[i].latLng.lat, gg.markersRcv[i].latLng.lng);
                wholeBounds.extend(point);
                gg.createMarker(point,gg.markersRcv[i].descr);
            };
            var routeOptions = {
                strokeColor: '#0000A0',
                strokeOpacity: 1.0,
                strokeWeight: 3,
                path: routePoints,
                map: gg.map
            };
            gg.routeLine = new google.maps.Polyline(routeOptions);
            gg.setRouteLength();
            gg.routeEditListeners();
        }else{
            var lat = $.cookie('lat') * 1;
            var lng = $.cookie('lng') * 1;
            var zoom = $.cookie('zoom') * 1;
            if (isNaN(lat && lng && zoom)) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        map.setCenter(initialLocation);
                    }, function() {
                        map.setCenter(initialLocation);
                    });
                }
            } else {
                initialLocation = new google.maps.LatLng(lat, lng);
            }
            var routeOptions = {
                strokeColor: '#0000A0',
                strokeOpacity: 1.0,
                strokeWeight: 3
            };
        gg.routeLine = new google.maps.Polyline(routeOptions);
        gg.routeLine.setMap(gg.map);
        }
        
        if (wholeBounds!==undefined) {
            gg.map.fitBounds(wholeBounds);
        }
        
        
        google.maps.event.addListener(map, 'idle', gg.setCookies);
        var modes = $('.btn-toolbar')[0];
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);
        $('#mode-selector').removeClass('hidden').addClass('show');
        $('#del-route').click(gg.delRoute);

        $('input[name="obj-mode"]').change(function() {
            if ($(this).parent().hasClass('active')) {
                setTimeout("$('.btn.hidden').click()", 10);
            } else {
                gg[$(this).val()]();
            }

        });
        
    };

    google.maps.event.addDomListener(window, 'load', gg.initialize);
}(window.gg = window.gg || {}, jQuery));
